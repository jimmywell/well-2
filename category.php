<?php
    global $wp_query;
    get_header();
?>
	<iframe id="height-change-listener" role="none" tabindex="-1" src="about:blank"></iframe>
	<section id="hero_1-0" class="comp hero image-bleed" data-tracking-container="true">
		<div class="hero-container">
			<div class="g g-two-up">
				<div class="g-main">
					<h1 class="hero-title">
						<?php single_cat_title(); ?>
					</h1>
				</div>
				<div class="loc secondary g-secondary">
					<div id="hero-image_1-0" class="comp hero-image mntl-block">
						<img src="<?php the_field('background_image', get_queried_object()) ?>" data-expand="300">
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="l-left-sidebar ">
		<div class="loc sidebar l-sidebar">
			<div id="breadcrumbs-list_1-0" class="comp is-full is-expandable breadcrumbs-list" data-tracking-container="true">
				<ul class="breadcrumbs-list-list">
					<li class="breadcrumbs-list-item">
						<a href="#fitness-beginners-overview-4581864" data-ordinal="1" class="breadcrumbs-list-link">
							Beginners
							<svg class="breadcrumbs-list-icon icon-empty-caret">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-empty-caret"></use>
							</svg>
						</a>
					</li>
					<li class="breadcrumbs-list-item">
						<a href="#fitness-motivation-overview-4581863" data-ordinal="2" class="breadcrumbs-list-link">
							Motivation
							<svg class="breadcrumbs-list-icon icon-empty-caret">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-empty-caret"></use>
							</svg>
						</a>
					</li>
					<li class="breadcrumbs-list-item">
						<a href="#fitness-sports-nutrition-overview-4581862" data-ordinal="3" class="breadcrumbs-list-link">
							Sports Nutrition
							<svg class="breadcrumbs-list-icon icon-empty-caret">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-empty-caret"></use>
							</svg>
						</a>
					</li>
					<li class="breadcrumbs-list-item">
						<a href="#fitness-trends-overview-4581861" data-ordinal="4" class="breadcrumbs-list-link">
							Fitness Trends
							<svg class="breadcrumbs-list-icon icon-empty-caret">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-empty-caret"></use>
							</svg>
						</a>
					</li>
					<li class="breadcrumbs-list-item is-collapsed">
						<a href="#running-overview-4581850" data-ordinal="5" class="breadcrumbs-list-link">
							Running
							<svg class="breadcrumbs-list-icon icon-empty-caret">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-empty-caret"></use>
							</svg>
						</a>
					</li>
					<li class="breadcrumbs-list-item is-collapsed">
						<a href="#strength-overview-4581846" data-ordinal="6" class="breadcrumbs-list-link">
							Strength
							<svg class="breadcrumbs-list-icon icon-empty-caret">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-empty-caret"></use>
							</svg>
						</a>
					</li>
					<li class="breadcrumbs-list-item is-collapsed">
						<a href="#walking-overview-4581844" data-ordinal="7" class="breadcrumbs-list-link">
							Walking
							<svg class="breadcrumbs-list-icon icon-empty-caret">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-empty-caret"></use>
							</svg>
						</a>
					</li>
					<li class="breadcrumbs-list-item is-collapsed">
						<a href="#workouts-4157162" data-ordinal="8" class="breadcrumbs-list-link">
							Workouts
							<svg class="breadcrumbs-list-icon icon-empty-caret">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-empty-caret"></use>
							</svg>
						</a>
					</li>
					<li class="breadcrumbs-list-item is-collapsed">
						<a href="#cardio-4157141" data-ordinal="9" class="breadcrumbs-list-link">
							Cardio
							<svg class="breadcrumbs-list-icon icon-empty-caret">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-empty-caret"></use>
							</svg>
						</a>
					</li>
					<li class="breadcrumbs-list-item is-collapsed">
						<a href="#flexibility-and-stretching-4157128" data-ordinal="10" class="breadcrumbs-list-link">
							Flexibility
							<svg class="breadcrumbs-list-icon icon-empty-caret">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-empty-caret"></use>
							</svg>
						</a>
					</li>
					<li class="breadcrumbs-list-item is-collapsed">
						<a href="#yoga-4157113" data-ordinal="11" class="breadcrumbs-list-link">
							Yoga
							<svg class="breadcrumbs-list-icon icon-empty-caret">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-empty-caret"></use>
							</svg>
						</a>
					</li>
					<li class="breadcrumbs-list-item is-collapsed">
						<a href="#pilates-4157097" data-ordinal="12" class="breadcrumbs-list-link">
							Pilates
							<svg class="breadcrumbs-list-icon icon-empty-caret">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-empty-caret"></use>
							</svg>
						</a>
					</li>
					<li class="breadcrumbs-list-item is-collapsed">
						<a href="#fitness-tools-and-equipment-4157144" data-ordinal="13" class="breadcrumbs-list-link">
							Tools and Equipment
							<svg class="breadcrumbs-list-icon icon-empty-caret">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-empty-caret"></use>
							</svg>
						</a>
					</li>
					<li class="breadcrumbs-list-item is-collapsed">
						<a href="#fitness-health-and-safety-4157143" data-ordinal="14" class="breadcrumbs-list-link">
							Health and Safety
							<svg class="breadcrumbs-list-icon icon-empty-caret">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-empty-caret"></use>
							</svg>
						</a>
					</li>
					<li class="breadcrumbs-list-item is-collapsed">
						<a href="#special-features-4796746" data-ordinal="15" class="breadcrumbs-list-link">
							Special Features
							<svg class="breadcrumbs-list-icon icon-empty-caret">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-empty-caret"></use>
							</svg>
						</a>
					</li>
				</ul>
				<a href="#fitness-4156989" class="text-btn">
					View More
					<svg class="text-btn-icon icon-circle-arrow-down">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-circle-arrow-down"></use>
					</svg>
				</a>
			</div>
		</div>
		<div class="loc content l-main">
			<?php get_template_part('template/sticky-posts/sticky', 'posts'); ?>
			<section id="article-list_1-0" class="comp article-list" data-tracking-container="true">
				<div class="loc content section-body">
					<ul id="block-list_1-0" class="comp g g-three-up block-list" data-chunk="24">
                        <?php
                            query_posts([
                                'post__not_in' => get_option( 'sticky_posts' )
                            ]);
                            while (have_posts()):
                                the_post();
                                get_template_part('template/loop/content');
                            endwhile;
                        ?>
					</ul>
                    <?php if($wp_query->max_num_pages > 1): ?>
					<a href="#" class="btn-link" aria-label="View More">
						<button class="btn btn-divider btn-dark" id="divider-button">
							<div class="btn-divider-inner">
								<span>View More</span>
								<svg class="icon icon-circle-arrow-down btn-icon">
									<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-circle-arrow-down"></use>
								</svg>
							</div>
						</button>
					</a>
                    <?php endif; ?>
				</div>
			</section>
			<div id="native-channel_1-0" class="comp native-channel native-homepage mntl-gpt-adunit gpt native " style="">
				<div id="native-channel" class="wrapper">
					<div id="google_ads_iframe_/479/verywellfit/vfit_fitness/native-channel_0__container__" style="border: 0pt none; width: 1px; height: 3px;"></div>
				</div>
			</div>
		</div>
	</div>
	<div id="leaderboard-footer_1-0" class="comp has-right-label has-left-label leaderboard-footer leaderboard mntl-flexible-leaderboard mntl-flexible-ad mntl-gpt-adunit gpt leaderboard " style="">
		<div id="leaderboard2" class="wrapper">
			<div id="google_ads_iframe_/479/verywellfit/vfit_fitness/leaderboard2_0__container__" style="border: 0pt none; width: 728px; height: 91px;"></div>
		</div>
	</div>
<?php get_footer(); ?>