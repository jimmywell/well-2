<?php

function n2t_get_top_category($post_id = null, $level = 3){

    if($post_id == null){
        $post_id = get_the_ID();
    }

    $categories = wp_get_post_categories($post_id);

    if(!empty($categories) && isset($categories[0])){
        $i = 0;
        $catid = $categories[0];
        while ($catid) {
            $cat = get_category($catid); // get the object for the catid
            $catid = $cat->category_parent; // assign parent ID (if exists) to $catid
              // the while loop will continue whilst there is a $catid
              // when there is no longer a parent $catid will be NULL so we can assign our $catParent
            $catParent = $cat->cat_ID;
            $i++;
            if($i == $level){
                return $catParent;
            }
        }
    } else {
        return false;
    }
}

function n2t_theme_init() {
    $labels = array(
        'name'                  => _x( 'Trending Topics', 'Post type general name', 'recipe' ),
        'singular_name'         => _x( 'Trending Topic', 'Post type singular name', 'recipe' ),
        'menu_name'             => _x( 'Trending Topics', 'Admin Menu text', 'recipe' ),
        'name_admin_bar'        => _x( 'Trending Topic', 'Add New on Toolbar', 'recipe' ),
        'add_new'               => __( 'Add New', 'trending topic' ),
        'add_new_item'          => __( 'Add New trending topic', 'trending topic' ),
        'new_item'              => __( 'New trending topic', 'trending topic' ),
        'edit_item'             => __( 'Edit trending topic', 'trending topic' ),
        'view_item'             => __( 'View trending topic', 'trending topic' ),
        'all_items'             => __( 'All trending topics', 'trending topic' ),
        'search_items'          => __( 'Search trending topics', 'trending topic' ),
        'parent_item_colon'     => __( 'Parent trending topics:', 'trending topic' ),
        'not_found'             => __( 'No trending topics found.', 'trending topic' ),
        'not_found_in_trash'    => __( 'No trending topics found in Trash.', 'trending topic' ),
        'featured_image'        => _x( 'Trending Topic Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'trending topic' ),
        'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'trending topic' ),
        'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'trending topic' ),
        'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'trending topic' ),
        'archives'              => _x( 'Trending Topic archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'trending topic' ),
        'insert_into_item'      => _x( 'Insert into trending topic', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'trending topic' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this trending topic', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'trending topic' ),
        'filter_items_list'     => _x( 'Filter trending topics list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'trending topic' ),
        'items_list_navigation' => _x( 'Trending Topics list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'trending topic' ),
        'items_list'            => _x( 'Trending Topics list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'trending topic' ),
    );
    $args = array(
        'labels'             => $labels,
        'description'        => 'Trending Topic custom post type.',
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'trending-topic' ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => 20,
        'supports'           => array( 'title' ),
        'show_in_rest'       => true
    );

    register_post_type( 'trending_topic', $args );
}
add_action( 'init', 'n2t_theme_init' );

add_action('after_setup_theme', 'n2t_theme_setup');
function n2t_theme_setup(){
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );
}

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

}

add_action('acf/init', 'my_acf_blocks_init');
function my_acf_blocks_init() {

    // Check function exists.
    if( function_exists('acf_register_block_type') ) {

        // Register a posts block.
        acf_register_block_type(array(
            'name'              => 'posts',
            'title'             => __('Posts By Category'),
            'description'       => __('Posts by category block.'),
            'render_template'   => 'template/blocks/posts/posts.php',
            'category'          => 'common',
            'mode'              => 'edit'
        ));
        // Register a post block.
        acf_register_block_type(array(
            'name'              => 'post',
            'title'             => __('Sticky Posts'),
            'description'       => __('Display all sticky posts.'),
            'render_template'   => 'template/sticky-posts/sticky-posts.php',
            'category'          => 'common',
            'mode'              => 'edit'
        ));
        // Register a mailchimp block.
        acf_register_block_type(array(
            'name'              => 'mailchimp',
            'title'             => __('MailChimp'),
            'description'       => __('MailChimp Form'),
            'render_template'   => 'template/blocks/mailchimp/mailchimp.php',
            'category'          => 'common',
            'mode'              => 'edit'
        ));
        // Register a categories block.
        acf_register_block_type(array(
            'name'              => 'categories',
            'title'             => __('Categories Grid'),
            'description'       => __('Display selected categories as grid layout'),
            'render_template'   => 'template/blocks/categories/categories.php',
            'category'          => 'common',
            'mode'              => 'edit'
        ));
    }
}

add_action('wp_enqueue_scripts', 'n2t_print_scripts');
function n2t_print_scripts(){
    wp_enqueue_script('jquery');
    if(is_single() || is_category() || is_archive()){
        //wp_enqueue_style('single', get_template_directory_uri() . '/assets/single.css');
    }
    wp_enqueue_style('main', get_stylesheet_uri());
}