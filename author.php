<?php
    global $wp_query;
    $author = get_queried_object();
    get_header();
?>
<div id="bio-header_1-0" class="comp bio-header mntl-block">
	<figure id="bio-header__image_1-0" class="comp figure-small bio-header__image figure-article mntl-block">
		<div id="figure-article__media_1-0" class="comp figure-article__media mntl-block">
			<div class="img-placeholder" style="padding-bottom:100.0%;">
				<img src="https://www.verywellfit.com/thmb/8WcA4WmHW_MKjwqw73k9HbqJkPA=/500x350/filters:no_upscale():max_bytes(150000):strip_icc():format(webp)/PaigeNew-594a893c3df78c537b843956.jpg" alt="Paige Waehner" class=" figure-article__image mntl-primary-image ">
			</div>
		</div>
	</figure>
	<div id="bio-header__details_1-0" class="comp bio-header__details mntl-block">
		<h1 id="bio-details__name_1-0" class="comp bio-details__name mntl-text-block">
			<?php the_author_meta('display_name', $author->ID); ?>
		</h1>
		<div id="bio-details__role_1-0" class="comp bio-details__role mntl-block">
			<span id="bio-details__title_1-0" class="comp bio-details__title mntl-text-block">Personal Trainer, Fitness Writer</span>
		</div>
		<div id="mntl-block_1-0" class="comp bio-details__ex-ed mntl-block">
			<h2 id="mntl-text-block_4-0" class="comp bio-details__ex-ed__title mntl-text-block">
				Expertise
			</h2>
			<span id="bio-expertise__items_1-0" class="comp bio-details__ex-ed__items bio-expertise__items mntl-text-block">Exercise</span>
		</div>
		<div id="mntl-block_2-0" class="comp bio-details__ex-ed mntl-block">
			<h2 id="mntl-text-block_5-0" class="comp bio-details__ex-ed__title mntl-text-block">
				Education
			</h2>
			<span id="bio-education__items_1-0" class="comp bio-details__ex-ed__items bio-education__items mntl-text-block"> University of Memphis</span>
		</div>
		<div id="social-follow_2-0" class="comp extended social-follow mntl-block">
			<div id="social-follow__intro_2-0" class="comp social-follow__intro mntl-text-block"></div>
			<ul id="social-follow__list_2-0" class="comp social-follow__list mntl-block">
				<li id="social-follow__item_2-0" class="comp social-follow__item social-follow__btn mntl-block">
					<a href="https://www.facebook.com/paigewaehnerexercise" target="_blank" rel="noopener" id="social-follow__link_2-0" class=" social-follow__link mntl-text-link social-follow__link--facebook">
						<span id="mntl-text-block_6-0" class="comp is-vishidden mntl-text-block">
						facebook</span>
						<svg class="icon icon-facebook ">
							<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-facebook"></use>
						</svg>
					</a>
				</li>
				<li id="social-follow__item_2-0-1" class="comp social-follow__item social-follow__btn mntl-block">
					<a href="https://www.linkedin.com/in/paige-waehner-a60b8b2a" target="_blank" rel="noopener" id="social-follow__link_2-0-1" class=" social-follow__link mntl-text-link social-follow__link--linkedin">
						<span id="mntl-text-block_6-0-1" class="comp is-vishidden mntl-text-block">
						linkedin</span>
						<svg class="icon icon-linkedin ">
							<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-linkedin"></use>
						</svg>
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div id="bio-content_1-0" class="comp expert-content bio-content">
	<h2 class="bio-content__header">Highlights</h2>
	<ul>
		<li>Personal trainer certified through the <a href="https://www.acefitness.org/" data-component="link" data-source="inlineLink" data-type="externalLink" data-ordinal="1" target="_blank" rel="noopener">American Council on Exercise</a></li>
		<li>Trains clients at <a href="http://www.eatwellgetstrong.com/fitness.html" data-component="link" data-source="inlineLink" data-type="externalLink" data-ordinal="2" rel="nofollow noopener" target="_blank">NutriFit Personal Training Studio</a> in the Chicago suburbs</li>
		<li>Co-author of <a href="https://books.google.com/books/about/The_Buzz_on_Exercise_Fitness.html?id=Eiuguu2RsewC" data-component="link" data-source="inlineLink" data-type="externalLink" data-ordinal="3" target="_blank" rel="noopener">The Buzz on Exercise &amp; Fitness</a> and the ebook FabJob <a href="https://fabjob.com/program/become-personal-trainer/" data-component="link" data-source="inlineLink" data-type="externalLink" data-ordinal="4" rel="nofollow noopener" target="_blank">Guide to Become a Personal Trainer</a></li>
	</ul>
	<h2 class="bio-content__header">Experience</h2>
	<p>Paige Waehner is a former writer for Verywell Fit covering exercise. Along with being a certified personal trainer for more than 16 years, Paige is the co-author of "The Buzz on Exercise &amp; Fitness," and author of the e-book "Guide to Become a Personal Trainer." Paige has written articles for Desert Paradise, Pregnancy Magazine, Runner's World and many other websites, newsletters and magazines.</p>
	<h2 class="bio-content__header">Education</h2>
	<p>Paige is certified for personal training through the American Council on Exercise. She also holds a Bachelor's Degree from the University of Memphis.</p>
	<div class="bio-content__separator"></div>
	<h2 class="bio-content__header">Verywell Fit Editorial Process</h2>
	<p>Verywell Fit is an award-winning online resource that empowers you to reach your diet and fitness goals. We take a human approach to health and wellness content and reach more than 120 million readers annually.</p>
	<p>Our editorial team includes writers, editors, and fact checkers who make sure our information is clear, accurate, and aligned with our <a href="/verywell-fit-core-values-4690420">core values</a> so you can make confident choices for your life.</p>
	<p>Our writers are notable voices in their respective disciplines, including registered dietitians, certified personal trainers, fitness coaches, board-certified physicians, and health journalists. These individuals are specifically selected for their extensive knowledge and real-world experience, as well as their ability to communicate complex information in a clear, helpful, and unbiased way.</p>
	<p>Our team of qualified and experienced fact checkers provide a critical step in our commitment to content integrity. Fact checkers rigorously review articles for accuracy, relevance, and timeliness. We use only the most current and reputable primary references, including peer-reviewed medical journals, government organizations, academic institutions, and advocacy associations.</p>
	<a href="/our-editorial-process-4778011" class="mntl-text-link btn btn-padded">
		<span class="link__wrapper">Learn More</span>
		<svg class="icon icon-circle-arrow-right">
			<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-circle-arrow-right"></use>
		</svg>
	</a>
</div>
<section id="bio-article-list_1-0" class="comp bio-article-list article-list">
	<span class="section-title">More From <?php the_author_meta('display_name', $author->ID); ?></span>
	<div class="loc content section-body">
		<ul id="block-list_1-0" class="comp g g-four-up block-list" data-chunk="24">
			<?php
                while(have_posts()):
                    the_post();
                get_template_part('template/loop/content');
                endwhile;
            ?>
		</ul>
        <?php if($wp_query->max_num_pages > 1): ?>
		<a href="#" class="btn-link" aria-label="View More">
			<button class="btn btn-divider btn-dark" id="divider-button">
				<div class="btn-divider-inner">
					<span>View More</span>
					<svg class="icon icon-circle-arrow-down btn-icon">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-circle-arrow-down"></use>
					</svg>
				</div>
			</button>
		</a>
        <?php endif; ?>
	</div>
</section>
<?php get_footer(); ?>