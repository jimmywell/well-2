<?php

/**
 * Posts Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'n2t-block-posts-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'n2t-block-posts';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assign defaults.
$category = get_category(get_field('category')) ?: 1;
$number_of_posts = get_field('number_of_posts') ?: 1;
$query = new WP_Query([
    'cat' => $category->term_id,
    'posts_per_page' => $number_of_posts
]);
if($query->have_posts()):
?>
<div id="<?php echo $id; ?>" class="comp article-group mntl-block <?php echo $className; ?>">
    <a href="<?php echo get_category_link($category->term_id); ?>" id="heading-block_<?php echo $block['id']; ?>" class=" heading-block mntl-text-link heading-block-health">
        <span class="link__wrapper"><?php echo $category->name; ?></span>
        <svg class="icon icon-circle-arrow-right text-btn-icon">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-circle-arrow-right"></use>
        </svg>
    </a>
    <ul id="block-list_<?php echo $id; ?>" class="comp g g-two-four block-list" data-chunk="">
        <?php while($query->have_posts()): $query->the_post(); ?>
        <li class="g-item block-list-item">
            <a id="post_block_<?php the_ID(); ?>" class="comp block--small block" rel="" data-doc-id="4843025" href="<?php the_permalink(); ?>" data-ordinal="1">
                <div class="block__media">
                    <div class="img-placeholder" style="padding-bottom:66.6%;">
                        <?php the_post_thumbnail('large', ['class'=> 'block__img lazyloaded']); ?>
                    </div>
                </div>
                <div class="block__content" data-kicker="Weight Management">
                    <div class="block__title">
                        <span><?php the_title(); ?></span>
                    </div>
                    <div class="block__byline"> <?php the_field('by_line', get_the_ID()); ?></div>
                </div>
            </a>
        </li>
        <?php endwhile; ?>
    </ul>
</div>
<?php endif;wp_reset_query(); ?>