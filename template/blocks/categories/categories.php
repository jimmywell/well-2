<?php

/**
 * Posts Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'n2t-block-posts-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'n2t-block-posts';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
$categories = get_field('categories');

if(!empty(categories)):
?>
<div id="grid-nav_1-0" class="comp grid-nav">
    <ul class="grid-nav-list g g-two-three-six">
        <?php foreach ($categories as $category): ?>
        <li class="g-item">
            <a href="<?php echo get_category_link($category); ?>" class="grid-nav-link">
                <div class="g-image-wrapper">
                    <img src="<?php the_field('featured_image', $category) ?>" alt="<?php echo $category->name; ?>">
                </div>
                <h3 class="grid-nav-title"><?php echo $category->name; ?></h3>
            </a>
        </li>
        <?php endforeach; ?>
    </ul>
</div>
<?php endif; ?>