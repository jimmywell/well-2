<section id="hero_1-0" class="comp hero hero-grid-nav">
    <div class="hero-container">
        <div class="g g-two-up">
            <div class="g-main">
                <h1 class="hero-title">
                    Know&nbsp;more. Be&nbsp;healthier.
                </h1>
                <div id="general-search_2-0" class="comp general-search">
                    <form class="general-search-form" role="search" action="<?php echo home_url()  ?>" method="get" data-suggestion="verywell">
                        <div class="input-group">
                            <button class="btn btn-submit">
                                <span class="is-vishidden">Search</span>
                                <svg class="icon icon-magnifying-glass">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-magnifying-glass"></use>
                                </svg>
                            </button>
                            <button class="btn btn-clear is-hidden">
                                <span class="is-vishidden">Clear</span>
                                <svg class="icon icon-x">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-x"></use>
                                </svg>
                            </button>
                            <input type="text" name="s" id="search-input" class="general-search-input" placeholder="How can we help you?" aria-label="Search the site" required="required" value="" autocomplete="off">
                            <button class="btn btn-bright btn-go">GO</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="loc secondary g-secondary">
                <section id="tags-section_2-0" class="comp tags-section">
                    <h2 class="tags-section-title">Trending Topics</h2>
                    <ul id="tags-nav_2-0" class="comp tags-nav link-list mntl-block">
                        <li id="link-list-items_4-0" class="comp tags-nav-item link-list-items link-list-item" data-ordinal="1">
                            <a href="//www.verywellfit.com/why-yoga-transitions-are-important-4125531" class="link-list-link tags-nav-link" data-ordinal="1"> Yoga Transitions
                            </a>
                        </li>
                        <li id="link-list-items_4-0-1" class="comp tags-nav-item link-list-items link-list-item" data-ordinal="2">
                            <a href="//www.verywellfit.com/which-part-of-my-foot-should-i-land-on-when-running-2911127" class="link-list-link tags-nav-link" data-ordinal="2"> Running Tips
                            </a>
                        </li>
                        <li id="link-list-items_4-0-2" class="comp tags-nav-item link-list-items link-list-item" data-ordinal="3">
                            <a href="//www.verywellfit.com/how-to-make-veggies-taste-good-3496151" class="link-list-link tags-nav-link" data-ordinal="3"> Tasty Vegetables
                            </a>
                        </li>
                        <li id="link-list-items_4-0-3" class="comp tags-nav-item link-list-items link-list-item" data-ordinal="4">
                            <a href="//www.verywellfit.com/calorie-counting-dos-and-donts-3495627" class="link-list-link tags-nav-link" data-ordinal="4"> Calorie Counting
                            </a>
                        </li>
                    </ul>
                </section>
            </div>
        </div>
    </div>
</section>