<?php get_header(); ?>
    <section id="hero_1-0" class="comp hero hero-grid-nav">
        <div class="hero-container">
            <div class="g g-two-up">
                <div class="g-main">
                    <h1 class="hero-title">
                        Know&nbsp;more. Be&nbsp;healthier.
                    </h1>
                    <div id="general-search_2-0" class="comp general-search">
                        <form class="general-search-form" role="search" action="<?php echo home_url('/')  ?>" method="get" data-suggestion="verywell">
                            <div class="input-group">
                                <button class="btn btn-submit">
                                    <span class="is-vishidden">Search</span>
                                    <svg class="icon icon-magnifying-glass">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-magnifying-glass"></use>
                                    </svg>
                                </button>
                                <button class="btn btn-clear is-hidden">
                                    <span class="is-vishidden">Clear</span>
                                    <svg class="icon icon-x">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-x"></use>
                                    </svg>
                                </button>
                                <input type="text" name="s" id="search-input" class="general-search-input" placeholder="How can we help you?" aria-label="Search the site" required="required" value="" autocomplete="off">
                                <button class="btn btn-bright btn-go">GO</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="loc secondary g-secondary">
                    <section id="tags-section_2-0" class="comp tags-section">
                        <h2 class="tags-section-title">Trending Topics</h2>
                        <ul id="tags-nav_2-0" class="comp tags-nav link-list mntl-block">
                            <?php
                                $query = new WP_Query([
                                    'post_type' => 'trending_topic',
                                    'posts_per_page' => 5
                                ]);
                                if($query->have_posts()):
                                    while($query->have_posts()): $query->the_post();
                            ?>
                            <li id="link-list-items_<?php the_ID(); ?>" class="comp tags-nav-item link-list-items link-list-item" data-ordinal="1">
                                <a href="<?php the_permalink(); ?>" class="link-list-link tags-nav-link"> <?php the_title(); ?></a>
                            </li>
                            <?php endwhile; endif;wp_reset_query(); ?>
                        </ul>
                    </section>
                </div>
            </div>
        </div>
    </section>
    <?php the_post(); the_content(); ?>
<?php get_footer(); ?>